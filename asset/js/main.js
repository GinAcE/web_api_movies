const api = 'https://api.themoviedb.org/3/';
const api_key = 'c301130453848bacac7b0ad8bd96493c';
async function evtSubmit(e) {
    e.preventDefault();
    const strSearch = $('form input').val();
    const reqStr = `${api}search/movie?api_key=${api_key}&query=${strSearch}`;
    const reponse = await fetch(reqStr);
    const rs = await reponse.json();
    $('#main').empty();
    for (let i of rs.results) {
        $('#main').append(`
      <div class="col-md-3">
        <div class="well text-center">
          <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2/${i.poster_path}">
          <h5>${i.title}</h5>
          <a  class="btn btn-primary" href="./default.html?id=${i.id}">Review</a>
          <a  class="btn btn-primary" href="./default.html?id=${i.id}">Movie Details</a>
          <hr>
        </div>
      </div>
    `);
    }
  }




/**
*   get popular movies from specific page.
*   @param {number} page number of the page that you want to get the popular movie
*   @returns {Array} one array movie objects  
*/
async function getPopularMovies(page) {
    const reqStr = `${api}movie/popular?api_key=${api_key}&language=en-US&page=${page}`;
    const reponse = await fetch(reqStr);
    const movies = await reponse.json();
    return movies.results;
}




/** 
*   get length of a specific movie.
*   @param {number} id of the movie
*   @returns {object} {"hour":hour,"minute":minute} 
*/
async function getLengthMovie(id) {
    const reqStr = `${api}movie/${id}?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const movie = await reponse.json();
    let m = movie.runtime % 60;
    let h = (movie.runtime - m) / 60;
    return { "hour": h, "minute": m }
}
/** 
*   get cast and crew of a specific movie.
*   @param {number} id of the movie
*   @returns {JSON} ["id":id,"cast":[{Object},{Object},...],"crew":[{Object},{Object},...]] 
*/
async function getCreditsMovie(id) {
    const reqStr = `${api}movie/${id}/credits?api_key=${api_key}`;
    const reponse = await fetch(reqStr);
    const result = await reponse.json();
    return result;
}
/** 
*   get information of a specific person.
*   @param {number} person_id of the person
*   @returns {JSON} that hold all information 
*/
async function getPerson(person_id) {
    const reqStr = `${api}person/${person_id}?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const result = await reponse.json();
    return result;
}
/** 
*   Get the movie credits for a person.
*   @param {number} person_id of the person
*   @returns {JSON} that hold all information 
*/
async function getMovieCredits(person_id) {
    const reqStr = `${api}person/${person_id}/movie_credits?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const result = await reponse.json();
    return result;
}
/** 
*   get director of specific movie in the crew.
*   @param {Array} crew of the movie
*   @returns {Array} [] array hold all director of the movie 
*/
function getDirectorMovie(crew) {
    let results = [];
    for (let i of crew) {
        if (i.department == "Director") {
            results.push(i);
        }
    }
    return results;
}



/** 
*   loading the popular movies.
*/
async function loading() {
    document.title = "Popular Movies";
    $('#main').empty();
    addLoading('main');
    let page_number = getUrlVars().page == undefined ? 1 : parseInt(getUrlVars().page);
    $('#mainTitle').html('Popular Movies');
    const movie = await getPopularMovies(page_number);
    for (let i of movie) {
        await addOverviewMovie(i);
    }
    reloadPagination(page_number);
    removeLoading('main');
}

/** 
*   reaload the pagination of website.
*   @param {number}page_number page number of current website
*/
function reloadPagination(page_number) {
    $('#main').append(`<nav class="mx-auto" aria-label="...">
    <ul class="pagination">
        <li class="page-item disabled bg-dark">
          <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item">
           <a class="page-link"  href="?page=${page_number - 1}">${page_number - 1}</a>
        </li>
        <li class="page-item active" aria-current="page">
          <a class="page-link"  href="?page=${page_number}">${page_number}</a>
        </li>
        <li class="page-item">
          <a class="page-link"  href="?page=${page_number + 1}">${page_number + 1}</a></li>
        <li class="page-item">
          <a class="page-link" href="#">Next</a>
        </li>
    </ul>
  </nav>`)
}

/** 
*   add an movie overview to the popular movies
*   @param {object} movie overview information of an movie
*/
async function addOverviewMovie(movie) {
    
    let len = await getLengthMovie(movie.id);
    $('#main').append(`
    <div class=" mb-5 col-6 px-2">
        <div class="card shadow h-100" onclick="window.location.href='./default.html?id=${movie.id}'" style="hover:cursor" >
           <div class="row no-gutters">
              <div class="col-md-5">
                 <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2${movie.poster_path}" class="card-img" alt="...">
              </div>
              <div class="col-md-7">
                    <div class="card-body">
                        <h5 class="card-title"><b>${movie.title}</b></h5>
                        <p class="card-text overview"><small>${standOverview(movie.overview)}</small></p>
                        <small><b>Updated:</b> ${movie.release_date}</small><br/>
                        <small><b>popularity:</b> ${movie.popularity}</small><br/>
                        <small><b> count:</b> ${movie.vote_count}</small><br/>
                        <small><b>vote :</b> ${movie.vote_average}</small><br/>
                        <small><b>Length:</b> ${len.hour}h ${len.minute}m</small><br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
`)
}


/** 
*   add loading animation while wesite is loading.
*   @param {string}id  id of block that you want to add loading animation
*/
function addLoading(id) {

    $(`#${id}`).append(`
    <div class="position-absolute bg-white " text-align:center id="${id}Load" style="width:100%;height:100%;top:0;left:0;z-index:100">
    <div class="com">
            <div class="spinner-grow text-muted"></div>
            <div class="spinner-grow text-primary"></div>
            <div class="spinner-grow text-success"></div>
            <div class="spinner-grow text-info"></div>
            <div class="spinner-grow text-warning"></div>
            <div class="spinner-grow text-danger"></div>
            <div class="spinner-grow text-secondary"></div>
            <div class="spinner-grow text-dark"></div>
            <div class="spinner-grow text-light"></div>
            <div class="spinner-grow text-muted"></div>
            <div class="spinner-grow text-primary"></div>
            <div class="spinner-grow text-success"></div>
            <div class="spinner-grow text-info"></div>
            <div class="spinner-grow text-warning"></div>
            <div class="spinner-grow text-danger"></div>
            <div class="spinner-grow text-secondary"></div>
            <div class="spinner-grow text-dark"></div>
            <div class="spinner-grow text-light"></div>
            <div class="spinner-grow text-muted"></div>
            <div class="spinner-grow text-primary"></div>
            <div class="spinner-grow text-success"></div>
            <div class="spinner-grow text-info"></div>
            <div class="spinner-grow text-warning"></div>
            <div class="spinner-grow text-danger"></div>
            <div class="spinner-grow text-secondary"></div>
            <div class="spinner-grow text-dark"></div>
            <div class="spinner-grow text-light"></div>
            <div class="spinner-grow text-muted"></div>
            <div class="spinner-grow text-primary"></div>
            <div class="spinner-grow text-success"></div>
            <div class="spinner-grow text-info"></div>
            <div class="spinner-grow text-warning"></div>
            <div class="spinner-grow text-danger"></div>
            <div class="spinner-grow text-secondary"></div>
            <div class="spinner-grow text-dark"></div>
            <div class="spinner-grow text-light"></div>
  </div>
    </div>`);
}

/** 
*   remove loading animation when website loaded.
*   @param {string}id  id of block that you want to remove loading animation
*/
function removeLoading(id) {
    $(`#${id}Load`).remove();
}
/** 
*   limit the overview string.
*   @param {string}overview  original overview string
*   @returns {string} the string that is limited
*/
function standOverview(overview) {
    let strIndex = overview.indexOf(' ', 200);
    if (strIndex) {
        return overview.slice(0, strIndex) + "...";
    }
    return overview;
}
/**
 * get all variables in currently URL
 * @returns {Object} all variables in URL
 */
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
/**
 * format time of movie to get day,month,year from it
 * @param {string} time 
 * @returns {Object} { "year": year, "month": month, "day": day }
 */
function formatTime(time) {
    let [_, year, month, day] = /(\d{4})-(\d{1,2})-(\d{1,2})/.exec(time);
    switch (month) {
        case '1':
            month = "January";
            break;
        case '2':
            month = "February";
            break;
        case '3':
            month = "March";
            break;
        case '4':
            month = "April";
            break;
        case '5':
            month = "May";
            break;
        case '6':
            month = "June";
            break;
        case '7':
            month = "July";
            break;
        case '8':
            month = "August";
            break;
        case '9':
            month = "September";
            break;
        case '10':
            month = "October";
            break;
        case '11':
            month = "November";
            break;
        case '12':
            month = "December";
            break;
    }

    return { "year": year, "month": month, "day": day };
}
/**
 * format the runtime of the movie
 * @param {number} time
 * @return {Object} { "hour": h, "minute": m }
 */
function formatLengthMovie(time) {
    let m = time % 60;
    let h = (time - m) / 60;
    return { "hour": h, "minute": m }
}




/**
 * add top 6 billed cast of the movie
 * @param {Array} cast all cast of the movie 
 */
function addTopCast(cast) {
    let j = 0;
    for (let i of cast) {
        $('#cast').append(`
            <div class="col-2 px-1">
                <div class="card shadow">
                    <img src="https://image.tmdb.org/t/p/w138_and_h175_face/${i.profile_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><b>${i.name}</b></h5>
                      <p class="card-text">${i.character}</p>
                    </div>
                </div>
            </div>`);
        j = j + 1;
        if (j == 6) {
            break;
        }
    }

}
/**
 * add trailer from youtube of the movie
 * @param {Array} trailer all trailer object of the movie
 */
function addTrailer(trailer) {
    for (let i of trailer) {
        if (i.site == 'YouTube') {
            $('#trailer').append(`
            <iframe title="YouTube video player" class="youtube-player mx-auto" type="text/html" 
             width="640" height="390" src="http://www.youtube.com/embed/${i.key}"
             frameborder="0" allowFullScreen></iframe>
            `);
            break;
        }
    }
}


async function getReview(movie) {
    const reqStr = `${api}movie/${movie.id}/reviews?api_key=${api_key}`;
    const reponse = await fetch(reqStr);
    const movies = await reponse.json();
    $('#Reviews').empty();
    for (let i of rs.results) {
        $('#Reviews').append(`
        <div class=" mb-5 col-6 px-2">
        <div class="card shadow h-100" onclick="window.location.href='./default.html?id=${movie.id}'" style="hover:cursor" >
           <div class="row no-gutters">
              <div class="col-md-5">
                 <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2${movie.poster_path}" class="card-img" alt="...">
              </div>
              <div class="col-md-7">
                    <div class="card-body">
                        <h5 class="card-title"><b>${movie.title}</b></h5>
                        <p class="card-text overview"><small>${standOverview(movie.overview)}</small></p>
                        <small><b> ${movie.author}</b> ${movie.content}</small><br/>
                        <small>  <b>ID:</b>  ${movie.id}</small><br/>
                        <small>  <b>URL:</b>  ${movie.url}</small><br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
        `);
}



}

/**
 * loadDetail of the movie
 */
async function loadDetail() {

    let vars = getUrlVars();
    $('#main').empty();
    addLoading();
    const reqStr = `${api}movie/${vars.id}?api_key=${api_key}&language=en-US`;
    const reqCastStr = `${api}movie/${vars.id}/casts?api_key=${api_key}`
    const reqTrailerStr = `${api}movie/${vars.id}/videos?api_key=${api_key}&language=en-US`
    const reponse = await fetch(reqStr);
    const movie = await reponse.json();

    const reponseCast = await fetch(reqCastStr);
    const cast = await reponseCast.json();

    const reponseTrailer = await fetch(reqTrailerStr);
    const trailer = await reponseTrailer.json();


    document.title = `${movie.original_title}`;
    let release_date = formatTime(movie.release_date);
    let lenghtMovie = formatLengthMovie(movie.runtime);
    $('#main').append(
        `<div class=" mb-8 col-10 px-2 mx-auto">
            <div class="card shadow h-100" >
                <div class="row no-gutters">
                    <div class="col-md-">
                       <img src="https://image.tmdb.org/t/p/w300_and_h450_bestv2${movie.poster_path}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-7">
                        <div class="card-body">
                            <h4 class="card-title"><b>${movie.original_title} (${release_date.year})</b></h4>
                            <p class="card-text">${movie.overview}</p>
                            <small><b>popularity:</b> ${movie.popularity} </small><br/>
                            <small><b> vote_count:</b> ${movie.vote_count} </small><br/>
                            <small><b> vote_average:</b> ${movie.vote_average} </small><br/>
                            <small><b>Updated:</b> ${release_date.month} ${release_date.day} ${release_date.year}</small><br/>
                            <small><b>Length:</b> ${lenghtMovie.hour}h ${lenghtMovie.minute}m</small>
                            <div class="row col">
                            <p class='d-block mb-0'><small><b>Rating:</b></small></p>
                            <div class="col-9">
                              <div class="progress">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: ${movie.vote_average * 10}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">${movie.vote_average * 10}%</div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
    )
    addTopCast(cast.cast);
    addTrailer(trailer.results);
    removeLoading();
}
